enum Colors {
  aliceblue = '#f0f8ff',
  black = '#000',
  blue = '#2196f3',
  darkgrey = '#a9a9a9',
  grey = '#ccc',
  white = '#fff',
}

export default Colors;
