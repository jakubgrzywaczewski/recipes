export type ButtonType = {
  type: 'button' | 'submit' | 'reset' | undefined;
};
