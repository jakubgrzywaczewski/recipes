import styled from 'styled-components';
import Colors from 'constants/Colors';

const StyledButton = styled.button`
  cursor: pointer;
  background-color: ${Colors.black};
  color: ${Colors.grey};
  outline: none;
  border: 2px solid ${Colors.black};
  width: fit-content;
  padding: 5px 25px;
  font-size: 1.3rem;
  transition: background-color 0.2s ease-in-out, color 0.2s ease-in-out;
  opacity: 1;

  &:hover {
    background-color: ${Colors.darkgrey};
    color: ${Colors.black};
  }
`;

export default StyledButton;
