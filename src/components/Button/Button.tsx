import React, { ReactNode, HTMLAttributes } from 'react';
import StyledButton from './Button.styles';

interface IButtonProps extends HTMLAttributes<HTMLButtonElement> {
  children: ReactNode;
  disabled?: boolean;
  handleOnClick?: () => void;
  type?: 'button' | 'submit' | 'reset' | undefined;
}

const Button: React.FC<IButtonProps> = (props: IButtonProps) => {
  const { children, disabled, handleOnClick, type } = props;

  return (
    <StyledButton disabled={disabled} onClick={handleOnClick} type={type}>
      {children}
    </StyledButton>
  );
};

export default Button;
