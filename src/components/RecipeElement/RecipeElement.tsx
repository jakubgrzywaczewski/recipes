import React, { useState } from 'react';
import { Body, Header, Wrapper } from './RecipeElement.styles';

type RecipeElementType = {
  title: string;
  ingredients: string;
};

interface IRecipeElement {
  element: RecipeElementType;
}

const RecipeElement: React.FC<IRecipeElement> = (props: IRecipeElement) => {
  const {
    element: { title, ingredients },
  } = props;

  const [isOpen, setOpen] = useState(false);
  const handleOnClick = () => setOpen((open) => !open);

  return (
    <Wrapper>
      <Header isOpen={isOpen} onClick={handleOnClick}>
        {title}
      </Header>
      <Body isOpen={isOpen}>{ingredients}</Body>
    </Wrapper>
  );
};

export default RecipeElement;
