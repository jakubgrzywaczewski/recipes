import styled from 'styled-components';
import Colors from '../../constants/Colors';

interface IRecipeElement {
  isOpen: boolean;
}

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0px;
  background-color: ${Colors.white};
  border: 2px solid ${Colors.black};
  margin-bottom: 10px;
`;

const Header = styled.div`
  height: 30px;
  padding: 5px;
  display: flex;
  align-items: center;
  font-size: 1.3rem;
  background-color: ${Colors.black};
  color: ${Colors.white};
  cursor: pointer;
  border: 1px solid ${(props: IRecipeElement) => (props.isOpen ? Colors.darkgrey : Colors.black)};

  &:hover {
    border: 1px solid ${Colors.darkgrey};
  }
`;

const Body = styled.div`
  overflow: hidden;
  padding: ${(props: IRecipeElement) => (props.isOpen ? '5px' : '0')};
  height: ${(props: IRecipeElement) => (props.isOpen ? '100px' : '0')};
  visibility: ${(props: IRecipeElement) => (props.isOpen ? 'initial' : 'hidden')};
  transition: height 0.5s linear, padding 0.1s linear, visibility 0.1s linear;
`;

export { Body, Header, Wrapper };
