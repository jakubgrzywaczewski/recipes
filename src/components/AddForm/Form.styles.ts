import styled from 'styled-components';

const Label = styled.label`
  width: 100%;
  font-weight: bold;
  margin-bottom: 8px;
`;

const Wrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;

  input {
    width: 100%;
    height: 38px;
    padding: 2px 6px;
    margin-bottom: 16px;
  }
`;

export { Label, Wrapper };
