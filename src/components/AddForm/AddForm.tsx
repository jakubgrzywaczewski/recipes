import React from 'react';
import { useDispatch } from 'react-redux';
import { Form, Field } from 'react-final-form';

import TextInput from 'components/Input/TextInput';
import TextAreaInput from 'components/Input/TextArea';
import { addRecipe } from 'store/recipes/actions';
import Button from 'components/Button';
import { Label, Wrapper } from './Form.styles';

interface Values {
  recipeTitle?: string;
  ingredients?: string;
}

const required = (value: string) => (value ? undefined : 'Required');

const AddForm: React.FC = () => {
  const dispatch = useDispatch();

  const onSubmit = async (values: Values) => {
    const { ingredients = 'a', recipeTitle = 'b' } = values;

    dispatch(addRecipe(recipeTitle, ingredients));
  };

  return (
    <Form
      onSubmit={onSubmit}
      render={({ handleSubmit, submitting, pristine }) => (
        <form onSubmit={handleSubmit}>
          <Wrapper>
            <Label htmlFor="title">Title</Label>
            <Field<string>
              name="recipeTitle"
              id="recipeTitle"
              component={TextInput}
              placeholder="Recipe title"
              validate={required}
            />
          </Wrapper>

          <Wrapper>
            <Label htmlFor="ingredients">Ingredients</Label>
            <Field
              name="ingredients"
              id="ingredients"
              component={TextAreaInput}
              placeholder="Ingredients"
              validate={required}
            />
          </Wrapper>

          <Wrapper>
            <Button type="submit" disabled={submitting || pristine}>
              Submit
            </Button>
          </Wrapper>
        </form>
      )}
    />
  );
};

export default AddForm;
