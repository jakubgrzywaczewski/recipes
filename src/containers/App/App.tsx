import React, { lazy, Suspense } from 'react';

import Menu from 'containers/Menu';
import { Reset } from './Reset.styles';
import Global, { Main } from './App.styles';

const RecipeList = lazy(() => import('containers/RecipeList/RecipeList'));

const loader = () => <p>Loading...</p>;

const App: React.FC = () => (
  <Main>
    <Reset />
    <Global />
    <Suspense fallback={loader()}>
      <RecipeList />
    </Suspense>
    <Menu />
  </Main>
);

export default App;
