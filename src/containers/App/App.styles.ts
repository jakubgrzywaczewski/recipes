import styled, { createGlobalStyle } from 'styled-components';
import Colors from 'constants/Colors';

export default createGlobalStyle`
  * {
    font-family: 'Alata', sans-serif !important;
    font-size: 16px;
    box-sizing: border-box;
  }
`;

export const Main = styled.main`
  height: 100vh;
  width: 100vw;
  padding: 2rem;
  background-color: ${Colors.aliceblue};
`;
