import styled from 'styled-components';
import Colors from '../../constants/Colors';

const Wrapper = styled.main`
  display: flex;
  flex-direction: column;
  padding: 15px;
  background-color: ${Colors.grey};
  border: 2px solid ${Colors.black};
`;

export default Wrapper;
