import React, { lazy } from 'react';
import { useSelector } from 'react-redux';

import { RootState } from 'store/rootReducer';
import { Recipe } from 'store/recipes/types';
import Wrapper from './RecipeList.styles';

const RecipeElement = lazy(() => import('components/RecipeElement/RecipeElement'));

const recipeSelector = (state: RootState) => state.recipes;

const RecipeList: React.FC = () => {
  const state = useSelector(recipeSelector);

  return (
    <Wrapper>
      {(state as Recipe[]).map((item) => (
        <RecipeElement key={item.id} element={item}>
          {item.title} {item.ingredients}
        </RecipeElement>
      ))}
    </Wrapper>
  );
};

export default RecipeList;
