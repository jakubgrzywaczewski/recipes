import styled from 'styled-components';

const StyledMenu = styled.menu`
  margin-top: 30px;
  height: 50px;
  display: flex;
  justify-content: flex-end;
`;

export default StyledMenu;
