import React, { useState } from 'react';

import Button from 'components/Button';
import Modal from 'containers/Modal/Modal';
import AddForm from 'components/AddForm';
import StyledMenu from './Menu.styles';

const Menu: React.FC = () => {
  const [isModalOpen, setModalOpen] = useState<boolean>(false);

  const handleCloseModal = () => setModalOpen(false);
  const handleOpenModal = () => setModalOpen(true);

  return (
    <StyledMenu>
      <Modal open={isModalOpen} closeModal={handleCloseModal}>
        <AddForm />
      </Modal>
      <Button handleOnClick={handleOpenModal}>Add</Button>
    </StyledMenu>
  );
};

export default Menu;
