import styled from 'styled-components';
import Colors from 'constants/Colors';

const Wrapper = styled.div`
  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Card = styled.div`
  border: 2px solid ${Colors.black};
  position: relative;
  min-width: 500px;
  z-index: 10;
  margin-bottom: 100px;
  background: white;
  padding: 15px;
  box-shadow: 2px 2px 10px rgba(0, 0, 0, 0.3);
`;

const Overlay = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100vh;
  box-sizing: border-box;
  margin: 0;
  background-color: rgba(0, 0, 0, 0.4);
`;

export { Overlay, Card, Wrapper };
