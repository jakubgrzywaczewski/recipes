import React from 'react';
import { createPortal } from 'react-dom';
import Button from 'components/Button';
import { Overlay, Card, Wrapper } from './Modal.styles';

const portal = document.querySelector('#portal');

interface IModalProps {
  open: boolean;
  children: React.ReactNode;
  closeModal: () => void;
}

const Modal: React.FC<IModalProps | null> = (props: IModalProps) => {
  const { open, children, closeModal } = props;

  if (!open) return null;

  return portal
    ? createPortal(
        <Overlay>
          <Wrapper>
            <Card>
              <div>{children}</div>
              <Button handleOnClick={closeModal}>Close modal</Button>
            </Card>
          </Wrapper>
        </Overlay>,
        portal,
      )
    : null;
};

export default Modal;
