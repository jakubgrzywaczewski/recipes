import { combineReducers } from 'redux';

import { recipeReducer } from './recipes/reducer';

const rootReducer = combineReducers({
  recipes: recipeReducer,
});

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
