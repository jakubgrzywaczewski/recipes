import { Recipe } from './recipes/types';

const getState = (): any => {
  try {
    const serializedState = localStorage.getItem('recipes');
    if (serializedState === null) {
      return undefined;
    }

    return JSON.parse(serializedState);
  } catch (err) {
    return undefined;
  }
};

const saveState = (state: Recipe): void => {
  try {
    const existingState = getState() || [];

    existingState.push(state);
    localStorage.setItem('recipes', JSON.stringify(existingState));
  } catch {
    throw new Error('Error');
  }
};

export { getState, saveState };
