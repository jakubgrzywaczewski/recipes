import { createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';

import rootReducer from './rootReducer';
import { getState } from './localStorage';

const composedEnhancers = composeWithDevTools();
const initialState = { recipes: getState() };

const store = createStore(rootReducer, initialState, composedEnhancers);

export default store;
