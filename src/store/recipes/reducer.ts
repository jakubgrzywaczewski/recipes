import { ActionType } from 'typesafe-actions';

import { ADD_RECIPE, REMOVE_RECIPE, EDIT_RECIPE, Recipe } from './types';
import * as actions from './actions';

export type RecipeAction = ActionType<typeof actions>;

const initialState: Recipe[] = [];

export const recipeReducer = (state = initialState, action: RecipeAction): Recipe[] => {
  switch (action.type) {
    case ADD_RECIPE:
      return [...state, action.payload];

    case EDIT_RECIPE:
      return state.map((s) => (s.id === action.payload.id ? { ...s, ...action.payload } : s));

    case REMOVE_RECIPE:
      return state.filter((s) => {
        if (s.id !== action.payload.id) {
          return true;
        }
        return false;
      });

    default:
      return state;
  }
};
