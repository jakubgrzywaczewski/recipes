import cuid from 'cuid';
import { action } from 'typesafe-actions';

import { saveState } from 'store/localStorage';
import { RecipesActionType, Recipe, ADD_RECIPE, REMOVE_RECIPE, EDIT_RECIPE } from './types';

export const addRecipe = (title: string, ingredients: string): RecipesActionType => {
  const data: Recipe = {
    id: cuid(),
    ingredients,
    title,
  };

  saveState(data);

  return action(ADD_RECIPE, data);
};

export const edit = (newRecipe: Recipe): RecipesActionType => {
  return action(EDIT_RECIPE, newRecipe);
};

export const remove = (recipe: Recipe): RecipesActionType => action(REMOVE_RECIPE, recipe);
