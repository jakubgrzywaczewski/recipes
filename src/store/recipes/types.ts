export const ADD_RECIPE = 'ADD_RECIPE';
export const EDIT_RECIPE = 'EDIT_RECIPE';
export const REMOVE_RECIPE = 'REMOVE_RECIPE';

export interface Recipe {
  id: string;
  ingredients: string;
  title: string;
}

export interface RecipeState {
  recipes: Recipe[];
}

interface AddRecipeAction {
  type: typeof ADD_RECIPE;
  payload: Recipe;
}

interface EditRecipeAction {
  type: typeof EDIT_RECIPE;
  payload: Recipe;
}

interface RemoveRecipeAction {
  type: typeof REMOVE_RECIPE;
  payload: Recipe;
}

export type RecipesActionType = AddRecipeAction | EditRecipeAction | RemoveRecipeAction;
